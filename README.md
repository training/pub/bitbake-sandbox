Bitbake Sandbox
===============

This is a minimal self-contained bitbake project, completly without any Open
Embedded or Yocto Project dependencies.

It allows experimenting with bitbake without any added complexity and features that
are added by OE or YP.

Usage
=====

Navigate your current directoy to this repository, the source the `sandbox-env`
like this:

```
. sandbox-env
```

Experiment by modifying the sandbox recipe located in `layer/sandbox.bb`.

Run the recipe:

```
bitbake sandbox
```

Have fun!
