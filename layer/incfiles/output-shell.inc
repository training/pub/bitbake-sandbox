bbplain() {
    echo "$*"
}

printenv() {
    set | sort | grep '^Var'
}

myfunction() {
    :
}

do_mytask[nostamp] = "1"
do_mytask() {
    # bitbake option '-v' enables shell xtrace option, disabling that:
    set +x

    myfunction
}
addtask mytask before do_build
