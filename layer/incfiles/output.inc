OUTPUT_VARS ??= ""

do_mytask[nostamp] = "1"
python do_mytask() {
    bb.plain("=== Recipe %s" % d.getVar("PN", True))
    for i in (d.getVar("OUTPUT_VARS", True) or "").split():
        bb.plain("%s = \"%s\"" % (i, d.getVar(i, True) or "None"))
}
addtask mytask before do_build
