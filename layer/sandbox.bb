DESCRIPTION = "A minimal sandbox recipe"

MYVAR = "my secret variable"

do_mytask[nostamp] = "1"
python do_mytask() {
    bb.plain("%s greets you!" % d.getVar("PN", True))
    bb.plain("%s" % d.getVar("MYVAR", True))
}
addtask mytask before do_build
