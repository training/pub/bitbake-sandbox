require incfiles/output.inc

DESCRIPTION = "Variable assignment examples"

VarA = "value 1"
VarA = "value 2"

VarB = "value 1"
VarB ?= "value 2"

VarC ?= "value 1"
VarC ??= "value 2"

VarD ?= "value 1"
VarD = "value 2"

VarE ??= "value 1"
VarE ?= "value 2"

VarF ??= "value 1"
VarF = "value 2"

VarG ??= "value 1"
VarG ??= "value 2"

VarH ?= "value 1"
VarH ?= "value 2"

OUTPUT_VARS = "VarA VarB VarC VarD VarE VarF VarG VarH"
