require incfiles/output.inc

DESCRIPTION = "Variable expansion examples"

VarA = "value 1"
VarB ?= "value 1"
VarC ??= "value 1"

VarD = "VarA=${VarA} VarB=${VarB} VarC=${VarC}"
VarE := "VarA=${VarA} VarB=${VarB} VarC=${VarC}"

VarA = "value 2"
VarB = "value 2"
VarC ?= "value 2"

VarF = "VarA=${VarA} VarB=${VarB} VarC=${VarC}"
VarG := "VarA=${VarA} VarB=${VarB} VarC=${VarC}"

VarA = "value 3"
VarB = "value 3"
VarC ?= "value 3"

VarH = "VarA=${VarA} VarB=${VarB} VarC=${VarC}"
VarI := "VarA=${VarA} VarB=${VarB} VarC=${VarC}"

OUTPUT_VARS = "VarD VarE VarF VarG VarH VarI"
