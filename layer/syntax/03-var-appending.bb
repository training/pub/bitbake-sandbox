require incfiles/output.inc

DESCRIPTION = "Appending/Prepending of variables"

VarA = "value1"
VarA += "value2"
VarA .= "value3"
VarA:append = "value4"
VarA:append = "value5"
VarA:append += "value6"

VarB = "value1"
VarB += "value2"
VarB:remove = "value3"
VarB:append = " value3"
VarB:append = " value4"
VarB:remove = "value4"

VarC = "value1"
VarC:append = " value2"
VarD := "VarC = ${VarC}"
VarC += "value3"

OUTPUT_VARS = "VarA VarB VarC VarD"
