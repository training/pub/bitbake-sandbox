require incfiles/output.inc

DESCRIPTION = "Using variable variable names"

VarA = "value1"
VarB = "append"
VarA:${VarB} = "value2"

VarC = "VarA=${VarA}"

VarD := "VarA=${VarA}"

VarB = "prepend"

VarE := "VarA=${VarA}"

Var${VarG} = "value3"
VarG = "F"
VarF = "value4"

OUTPUT_VARS = "VarA VarB VarC VarD VarE VarF VarG"
