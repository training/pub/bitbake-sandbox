require incfiles/output.inc

DESCRIPTION = "Variable override syntax examples"

OVERRIDES = "generic:common:specific:${DISTRO}:${ARCH}:${MACHINE}"

VarA = "default"
VarA:common = "common"
VarB := "${VarA}"
VarA:specific = "specific"
VarC := "${VarA}"

DISTRO = "poky"
ARCH = "x86-64"
MACHINE = "bulldozer"

VarD ??= "defaultOption"
VarD:debian = "debianOption"
VarD:poky = "pokyOption"
VarD:gentoo = "${VarD} gentooOption"

VarE := "VarD=${VarD}"

VarD:append:bulldozer = " bulldozerOption"
VarD:append:atom = " atomOption"

VarD:append:arm = " armOption"
VarD:append:x86-64 = " amd64Option"

VarF := "VarD=${VarD}"

DISTRO = "debian"
ARCH = "arm"
MACHINE = "mx6"

OUTPUT_VARS = "VarA VarB VarC DISTRO ARCH MACHINE VarD VarE VarF"
