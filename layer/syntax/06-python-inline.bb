require incfiles/output.inc

DESCRIPTION = "Python inline function example"

VarA = "${@'value %s' % 1}"
VarB = "${@'value %s' % str(1 + 1)}"

VarC = "${@'${VarA}'}"
VarD = "${VarB} ${@os.sys.version}"

VarE = "value 3 ${VarA}"
VarF = "${@d.getVar('VarE', False).upper()}"
VarG = "${@d.getVar('VarE', True).upper()}"

VarH := "\
 VarE='${VarE}'\
 VarF='${VarF}'\
 VarG='${VarG}'\
 VarE_python='${@d.getVar('VarE', True)}'\
"

VarE = "value 4 ${VarB}"

OUTPUT_VARS = "${@' '.join('Var' + chr(i) for i in range(ord('A'), ord('I')))}" 
