require incfiles/output-shell.inc

DESCRIPTION = "Bitbake shell function example"

VarA = "value1"
VarB = "value1"

myfunction() {
    bbplain "====1===="
    printenv

    VarA="${VarB}"

    bbplain "====2===="
    printenv

    VarB="value2"

    bbplain "====3===="
    printenv

    VarB="${@d.getVar("VarB")}"

    bbplain "====4===="
    printenv

}

export VarC = "value1"
export VarD = "value1"

myfunction:append() {
    VarC="value2"
    VarD="${VarC}"

    bbplain "====5===="
    printenv

    VarD="$VarC"

    bbplain "====6===="
    printenv

    VarE="value1"
    VarD="${VarE}"

    bbplain "====7===="
    printenv
}
