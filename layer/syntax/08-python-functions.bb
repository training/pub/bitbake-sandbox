require incfiles/output.inc

DESCRIPTION = "Python function example"

def count_up(d):
    value = int(d.getVar('_hidden_counter', True) or '0')
    value += 1
    value = str(value)
    d.setVar('_hidden_counter', value)
    return value

VarA = "value ${@count_up(d)}"
VarB = "value ${@count_up(d)}"
VarD := "${VarA} ${VarB}"
VarA:append = " ${@count_up(d)}"
VarA:append = " ${@count_up(d)}"
VarA:append = " ${@count_up(d)}"

OUTPUT_VARS = "VarA VarB VarD"
