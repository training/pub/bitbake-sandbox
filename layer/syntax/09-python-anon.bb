require incfiles/output.inc

DESCRIPTION = "Anonymous Python function example"

python () {
    d.setVar("VarB", d.getVar("VarA", True))

    d.appendVar("VarA", " 4")
    d.setVar("VarC", d.getVar("VarA", True))

    d.setVar("VarA:append", " 5")
    d.setVar("VarD", d.getVar("VarA", True))

    d.setVar("VarF", d.getVar("VarE", False).upper())
}

VarA = "value 1"
VarA += "2"
VarA:append = " 3"

VarE := "${VarA}"

OUTPUT_VARS = "VarA VarB VarC VarD VarE VarF"
