require incfiles/output.inc

CLASSINHERIT = "a"
VAR := "${CLASSINHERIT}"

inherit class-${CLASSINHERIT}

CLASSINHERIT = "b"

VAR:append:class-a = " class-a"
VAR:append:class-b = " class-b"

OUTPUT_VARS = "CLASS CLASSINHERIT VAR"
