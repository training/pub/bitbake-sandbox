# Add the tasks of this recipe, so that they are executed in this order:
# - Fetching
# - Configuring
# - Installing
# - Packaging / - Sysroot (They should be processed in parallel)
#
# Call this task via `bitbake -v 01-internal-order`

do_fetch[nostamp] = "1"
do_fetch() {
    set +x
    echo "Fetching ${PN}"
}

do_configure[nostamp] = "1"
do_configure() {
    set +x
    echo "Configuring ${PN}"
}

do_install[nostamp] = "1"
do_install() {
    set +x
    echo "Installing ${PN}"
}

do_package[nostamp] = "1"
do_package() {
    set +x
    echo "Packaging ${PN}"
}

do_sysroot[nostamp] = "1"
do_sysroot() {
    set +x
    echo "Sysroot ${PN}"
}

### Do this in 5 code lines under this line ###
