# 1. Make the `do_configure` task in this recipe dependant on the `do_sysroot`
# task of its DEPENDS.
#
# 2. Make sure that the `do_fetch` task is called after the `do_fetch` task in the
# `01-iternal-order` recipe and only that (Not on possibe other additions to
# the `DEPENDS` var).
#
# Call this task via `bitbake -v 02-external-order`

require 01-internal-order.bb

DEPENDS="01-internal-order"

### Do this in 2 codes lines under this line ###
