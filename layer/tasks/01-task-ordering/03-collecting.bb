# Make sure that the `do_rootfs` task is called after all `do_package` tasks.
#
# Call this task via `bitbake -v 03-collecting`

do_rootfs[nostamp]="1"
do_rootfs() {
    set +x
    echo "Rootfs ${PN}"
}

DEPENDS="02-external-order"

### Do this in 2 code lines under this line ###
