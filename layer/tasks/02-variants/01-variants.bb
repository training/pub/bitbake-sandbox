# Create two variants of this recipe using the `extend-a` and `extend-b` class.
# The `extend-a` variant should greet John, and the `extend-b` variant should
# greet Alice.
#
# Call them via:
# - `bitbake -v 01-variants`
# - `bitbake -v 01-variants-extend-a`
# - `bitbake -v 01-variants-extend-b`

NAME = "World"

do_greet[nostamp]="1"
do_greet() {
    set +x
    echo "Hello ${NAME}!"
}
addtask do_greet before do_build

### Do this in 3 code lines under this line ###
