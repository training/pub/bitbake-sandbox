#!/bin/sh

. ./sandbox-env

find layer -iname '*.bb' -exec basename {} \; | while read -r file; do
    bitbake -v "${file%%.bb}"
done
